<div class="certificate-container">
    <h1>{{ $certificate->course_name }}</h1>
    <p>Certificate: {{ $certificate->certificate_number }}</p>
    <p>Name: {{ $certificate->student_name }}</p>
    <p>Completion date: {{ $certificate->completion_date }}</p>
    <img src="data:image/png;base64, {!! base64_encode($qrCode) !!}" alt="QR Code">
</div>


<style>
    .certificate-container {
        font-family: 'Arial', sans-serif;
        max-width: 600px;
        margin: 40px auto;
        padding: 20px;
        background: white;
        border: 1px solid #ddd;
        box-shadow: 0 0 10px rgba(0,0,0,0.1);
        text-align: center;
    }

    .certificate-container h1 {
        color: #007bff;
        margin-bottom: 10px;
    }

    .certificate-container p {
        color: #333;
        font-size: 16px;
        line-height: 1.5;
    }

    .certificate-container img {
        margin-top: 20px;
    }
</style>
