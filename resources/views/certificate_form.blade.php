<form method="POST" action="{{ route('certificate.store') }}">
    @csrf
    <input type="text" name="certificate_number" placeholder="Certificate Number" required>
    <input type="text" name="course_name" placeholder="Course Name" required>
    <input type="text" name="student_name" placeholder="Student Name" required>
    <input type="date" name="completion_date" required>
    <button type="submit">Отримати сертифікат</button>
</form>

<style>
    body {
        font-family: 'Arial', sans-serif;
        background-color: #f4f4f9;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
        margin: 0;
    }

    form {
        background: white;
        padding: 20px;
        border-radius: 8px;
        box-shadow: 0 4px 6px rgba(0,0,0,0.1);
    }

    input[type="text"],
    input[type="date"] {
        width: calc(100% - 22px);
        padding: 10px;
        margin-top: 8px;
        margin-bottom: 16px;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
    }

    button {
        background-color: #007bff;
        color: white;
        border: none;
        padding: 10px 20px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
        border-radius: 4px;
        width: 100%;
    }

    button:hover {
        background-color: #0056b3;
    }
</style>
