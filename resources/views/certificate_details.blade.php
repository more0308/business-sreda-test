<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Certificate Details</title>
</head>
<body>
<h1>Certificate Details</h1>
<p><strong>Certificate Number:</strong> {{ $certificate->certificate_number }}</p>
<p><strong>Course Name:</strong> {{ $certificate->course_name }}</p>
<p><strong>Student Name:</strong> {{ $certificate->student_name }}</p>
<p><strong>Completion Date:</strong> {{ $certificate->completion_date }}</p>
</body>
</html>
