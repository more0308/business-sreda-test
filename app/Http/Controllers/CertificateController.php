<?php

namespace App\Http\Controllers;

use App\Http\Requests\CertificateStoreRequest;
use App\Models\Certificate;
use App\Services\PDFService;

class CertificateController extends Controller
{
    protected PDFService $PDFService;

    public function __construct(PDFService $PDFService)
    {
        $this->PDFService = $PDFService;
    }

    public function create()
    {
        return view('certificate_form');
    }

    public function store(CertificateStoreRequest $request)
    {
        $data = $request->validated();

        $certificate = Certificate::create($data);

        $pdf = $this->PDFService->generateCertificate($certificate);

        return $pdf->download("certificate-$certificate->id.pdf");
    }

    public function show(Certificate $certificate)
    {
        return view('certificate_details', compact('certificate'));
    }
}
