<?php

namespace App\Services;

use App\Models\Certificate;
use Barryvdh\DomPDF\Facade\Pdf;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class PDFService
{
    public function generateCertificate(Certificate $certificate) {
        $qrCode = QrCode::size(100)->generate(url(route('certificate.show', $certificate->id)));

        return PDF::loadView('certificate_template', [
            'certificate' => $certificate,
            'qrCode' => $qrCode
        ]);
    }
}
